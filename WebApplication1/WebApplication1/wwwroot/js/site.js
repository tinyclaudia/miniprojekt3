﻿// Write your Javascript code.
const markerSource = new ol.source.Vector();

var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');


var markerStyle = new ol.style.Style({
    image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        opacity: 0.75,
        src: 'https://openlayers.org/en/v4.6.4/examples/data/icon.png'
    }))
});

var overlay = new ol.Overlay({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
});


var map = new ol.Map({
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM(),
        }),
        new ol.layer.Vector({
            source: markerSource,
            style: markerStyle
        })
    ],
    overlays: [overlay],
    target: "map",
    view: new ol.View({
        center: [2342583.9581056964, 6845966.78415203],
        zoom: 12
    })
});

var geocoder = new Geocoder('nominatim', {
    provider: 'mapquest',
    key: '__some_key__',
    lang: 'pt-BR', //en-US, fr-FR
    placeholder: 'Search for ...',
    targetType: 'text-input',
    limit: 5,
    keepOpen: true
});

function addMarker(lon, lat) {
    var iconFeatures = [];
    //copied, todo
    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326',
            'EPSG:3857')),
        name: 'Null Island',
        population: 4000,
        rainfall: 500
    });

    markerSource.addFeature(iconFeature);
}

$(document).ready(function () {

    var select = new ol.interaction.Select();
    map.addInteraction(select);

    map.addControl(geocoder);

    geocoder.on('addresschosen', function (evt) {
        var feature = evt.feature;
        var coord = evt.coordinate;

        app.addMarker(feature, coord);
    });


    map.getViewport().addEventListener('contextmenu', function (evt) {
        evt.preventDefault();
        var lonLat = ol.proj.toLonLat(map.getEventCoordinate(evt));

       // var hdms = toStringHDMS(lonLat);

        content.innerHTML = '<p>Wielkość paczki:</p><form onsubmit="addMarker(' + lonLat[0] + ', ' + lonLat[1] + ')"><input type="text"><input type="submit" value="Zatwierdź"></form>';
        overlay.setPosition(map.getEventCoordinate(evt));

        

    })

    var select = new ol.interaction.Select();
    map.addInteraction(select);

    select.on('select',
        function (e) {
            
           // overlay.setPosition(e.coordinate);
        });

    closer.onclick = function () {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
    };
});